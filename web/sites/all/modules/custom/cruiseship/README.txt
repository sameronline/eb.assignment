Author: Samer Ali
Email: samer@sameronline.com

- This took around 3 hours to complete.
- I would recommend to set Errors to display to "Errors and warnings" @admin/config/development/logging before runing the
example as there are some notices at the db layer of D7
- I think I covered 100% of what you mentioned at the assigment with the DOB added.
- I was going to use the id field from {cruiseship_contest_codes} table at {cruiseship_contest_participate} but then i settled
on just using the plain code since time is limited and i didnot want to deal with joins etc.
- If you allowed constributed modules i would have not almost wrote anything and used features ;)
- at the begning i was going to use custom Entity but i really wanted Entity API https://www.drupal.org/project/entity module but you didnot allow it so i went direct with just punch of simple tables.
- If you would like to see how i handled the whole things in terms of git flow, please see https://bitbucket.org/sameronline/eb.assignment