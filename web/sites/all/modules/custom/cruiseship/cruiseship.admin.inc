<?php

/**
 * Implements hook_menu().
 */
function cruiseship_introductory_form() {
  $form = array();

  $values = variable_get('cruiseship_intro_text', _cruiseship_get_intro_default());

  $form['cruiseship_intro_text'] = array(
    '#type' => 'text_format',
    '#title' => t('Cruiseship Context Intro'),
    '#default_value' => $values['value'],
    '#required' => TRUE,
    '#format' => $values['format']
  );

  return system_settings_form($form);
}
