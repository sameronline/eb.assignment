<?php

define('CRUISESHIP_INTRO_TEXT_DEFAULT', 'Participate in the contest and win a <em>luxurious 1-week cruise on the river Nile!</em>');

function _cruiseship_get_intro_default() {
  return array(
    'value' => t(CRUISESHIP_INTRO_TEXT_DEFAULT),
    'format' => filter_default_format()
  );
}
