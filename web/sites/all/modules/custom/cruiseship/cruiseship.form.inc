<?php


/**
 * Cruise participation form
 *
 * @return array
 */
function cruiseship_participate_form() {
  $form = array();
  $intro_txt_raw = variable_get('cruiseship_intro_text', _cruiseship_get_intro_default());

  $form['IntroductoryText'] = array(
    '#type' => 'markup',
    '#markup' => check_markup($intro_txt_raw['value'], $intro_txt_raw['format'])
  );
  $form['code'] = array(
    '#title' => t('Contest code'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#maxlength' => 8,
    '#element_validate' => array('participate_code_validate')
  );
  $form['firstname'] = array(
    '#title' => t('Your first name'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#maxlength' => 255
  );
  $form['lastname'] = array(
    '#title' => t('Your last name'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#maxlength' => 255
  );
  $form['mail'] = array(
    '#title' => t('Your e-mail address'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#maxlength' => 255,
    '#element_validate' => array('participate_mail_validate')
  );
  $form['dob'] = array(
    '#title' => t('Date of birth'),
    '#type' => 'date',
    '#after_build' => array("cruiseship_format_dob")
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Participate'),
  );

  return $form;
}

/**
 * Validation the participate_form
 *
 * @see cruiseship_participate_form()
 *
 * @param $form
 * @param $form_state
 */
function cruiseship_participate_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  $p_mail_exists = db_select('cruiseship_contest_participate', 'ccp')
    ->fields('id')
    ->condition('mail', $form_state['values']['mail'])
    ->countQuery()
    ->execute()
    ->fetchField();
  if ($p_mail_exists) {
    form_set_error('[mail]', t("You cheater! This e-mail address @mail was used already.", array('@mail' => $form_state['values']['mail'])));
  }

  $p_code_exists = db_select('cruiseship_contest_participate', 'ccp')
    ->fields('id')
    ->condition('code', $form_state['values']['code'])
    ->countQuery()
    ->execute()
    ->fetchField();
  if ($p_code_exists) {
    form_set_error('[code]', t("\"You cheater! The code @code was used already.", array('@code' => $form_state['values']['code'])));
  }
}

/**
 * Submit handler for Contest Participate form
 *
 * @param $form
 * @param $form_state
 */
function cruiseship_participate_form_submit($form, &$form_state) {

  $values = $form_state['values'];
  $dob = $values['dob']['year'] . '-' . $values['dob']['month'] . '-' . $values['dob']['day'];
  $record = array(
    'code' => $values['code'],
    'firstname' => $values['firstname'],
    'lastname' => $values['lastname'],
    'mail' => $values['mail'],
    'created' => time(),
    'dob' => $dob
  );
  $query = db_insert('cruiseship_contest_participate')->fields(array(
    'code',
    'firstname',
    'lastname',
    'mail',
    'created',
    'dob'
  ));
  $query->values($record);


  try {
    $query->execute();
  } catch (PDOException $e) {
    watchdog('cruiseship', '%message', array('%message' => $e->getMessage()), WATCHDOG_ERROR);
    drupal_set_message(t('Sorry we are having technical difficulties, Webmaster has been notified'), 'error');
  }

  // everything is good, go forward
  $form_state['redirect'] = '<front>';
  drupal_set_message(t('Thank you for participating in Cruise Ship Contest'));
}

/**
 * #after_build function for dob field
 *
 * @see cruiseship_participate_form()
 *
 * @param $form_element
 * @param $form_state
 * @return mixed
 */
function cruiseship_format_dob($form_element, &$form_state) {
  // unset the default years options
  unset($form_element['year']['#options']);

  //
  $max_age = date('Y') - 100; // 100 years ago
  $min_age = date('Y'); // current year

  // Now we populate the array
  $form_element['year']['#options'] = array();
  foreach (range($max_age, $min_age) as $year) {
    $form_element['year']['#options'][$year] = $year;
  }

  return $form_element;
}

/**
 * Validation function for the email field element
 *
 * @see cruiseship_participate_form()
 *
 * @param $element
 * @param $form_state
 * @param $form
 */
function participate_mail_validate($element, &$form_state, $form) {
  $value = $element['#value'];
  if (!valid_email_address($value)) {
    form_error($element, t('Invalid email address @mail', array('@mail' => $value)));
  }
}

/**
 * Validation function for participate code element
 *
 * @see cruiseship_participate_form()
 *
 * @param $element
 * @param $form_state
 * @param $form
 */
function participate_code_validate($element, &$form_state, $form) {
  $value = $element['#value'];

  // validate only if the value was filled

  if (strlen($value)) {
    if (!preg_match('/[^a-z0-9]/i', $value) && strlen($value) != 8) {
      form_error($element, t('Invalid contest code, only length of 8 alphanumeric characters are allowed'));
      // exit here no need to continue for performance reasons and it's invalid anyway
      return;
    }

    // Check if code does exist already
    $code_exists = db_select('cruiseship_contest_codes', 'ccc')
      ->fields('code')
      ->condition('code', $value)
      ->countQuery()
      ->execute()
      ->fetchField();

    if (!$code_exists) {
      form_error($element, t('Unknown code @code. Please check the entered code, correct
any mistakes and try again.', array('@code' => $value)));
    }
  }
}
